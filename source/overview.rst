Overview
============================

DockDogs Mission Statement
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

DockDogs :sup:`®` maintains zero tolerance for bullying, harassment, and/or
unethical treatment of spectators, competitors, employees, and
canines. Everyone involved follows the rules and policies and ethical
guidelines which are the foundation of DockDogs :sup:`®` Worldwide.

Organizational Structure
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+----------------------+----------------------------+----------------+---------------------------------+
| Name                 | Position                   | Phone          | Email                           |
+======================+============================+================+=================================+
| DockDogs Worldwide   |                            | 330-241-4975   |                                 |
+----------------------+----------------------------+----------------+---------------------------------+
| Grant Reeves         | CEO                        | 330-241-4975   | grant.reeves@dockdogs.com       |
+----------------------+----------------------------+----------------+---------------------------------+
| Teresa Reeves        | General Manager            | 330-241-4975   | office@dockdogs.com             |
+----------------------+----------------------------+----------------+---------------------------------+
| Vicki Tighe          | Director of Affiliates     | 239.464.1859   | vicki.tighe@dockdogs.com        |
+----------------------+----------------------------+----------------+---------------------------------+
| Betsy Taylor         | SC-SW Regional Rep         | 337.371.8551   | betsy.taylor@dockdogs.com       |
+----------------------+----------------------------+----------------+---------------------------------+
| Joan Gunby           | NE Regional Rep            | 410.903.7137   | joan.gunby@dockdogs.com         |
+----------------------+----------------------------+----------------+---------------------------------+
| Linda Ruiz           | NC-NW Regional Rep         | 762.234.9538   | linda.ruiz@dockdogs.com         |
+----------------------+----------------------------+----------------+---------------------------------+
| Lisa Hudgens         | SE Regional Rep            | 865.406.5994   | lisa.hudgens@dockdogs.com       |
+----------------------+----------------------------+----------------+---------------------------------+
| Melissa Doren        | Canadian Regional Rep      | 647.686.1691   | melissa.doren@dockdogs.com      |
+----------------------+----------------------------+----------------+---------------------------------+
| Brian King           | IT/Marketing               | 330-241-4975   | brian.king@dockdogs.com         |
+----------------------+----------------------------+----------------+---------------------------------+
| Theresa Bufalino     | Accounting                 | 330-241-4975   | theresa.bufalino@dockdogs.com   |
+----------------------+----------------------------+----------------+---------------------------------+
| Linda Torson         | Administration/Elections   | 330-241-4975   | dd\_admin@dockdogs.com          |
+----------------------+----------------------------+----------------+---------------------------------+
| Sam Bruno            | Accounting                 | 330-241-4975   | sam.bruno@dockdogs.com          |
+----------------------+----------------------------+----------------+---------------------------------+
| Brian Sharenow       | Events                     | 330-241-4975   | brian.sharenow@dockdogs.com     |
+----------------------+----------------------------+----------------+---------------------------------+
| Sean Swearinger      | Operations Manager         | 330-241-4975   | sean.swearinger@dockdogs.com    |
+----------------------+----------------------------+----------------+---------------------------------+
